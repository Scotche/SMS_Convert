#!/Python27/python
# coding: utf-8

__author__ = 'fabien'

import xml.etree.ElementTree as ET
import datetime
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("file", help="xml file from your Windows Phone backup.")
args = parser.parse_args()


try:
    # http://effbot.org/zone/python-with-statement.htm
    # with is more safe to open file
    with open(args.file) as fh:

        # XML Backup file open
        tree = ET.parse(args.file)
        root = tree.getroot()

        # New XML SMS file
        file = open("convert_" + args.file, "w")

        count = 0

        for Message in root.findall("Message"):
            count = count + 1


        file.write("""<?xml version='1.0' encoding='UTF-8' standalone='yes' ?><!--File Created By SMS Converter--><?xml-stylesheet type="text/xsl" href="sms.xsl"?>""")
        file.write('<smses count="%d">' % count)


        for Message in root.findall("Message"):
            # On determine le type de message
            StrMessage = u'<sms protocol="0" '

            for IsIncoming in Message.findall("IsIncoming"):
                if (IsIncoming.text == "false"):
                    TypeMessage = "1"
                else:
                    TypeMessage = "2"

            StrMessage = StrMessage + u' type="' + TypeMessage + u'"'

            # Suivant le type de message on utilise le bon champ de contact
            if (TypeMessage == "1"):
                for Recepients in Message.findall("Recepients/string"):
                    if Recepients.text is None:
                        Insert = ""
                    else:
                        Insert = Recepients.text

                    StrMessage = StrMessage + u' address="' + Insert + '"'

            else:
                for Sender in Message.findall("Sender"):
                    if Sender.text is None:
                        Insert = ""
                    else:
                        Insert = Sender.text

                    StrMessage = StrMessage + u' address="' + Insert + u'"'


            # Récupération du texte du message (format unicode)
            # en respectant les " inclus dans le message
            for Body in Message.findall("Body"):
                if Body.text is None:
                    Insert = u""
                else:
                    Insert = Body.text.replace('"','\\"')

                StrMessage = StrMessage + u' subject="null" body="' + Insert + u'"'

            # Date du message (réduction du timestamp et conversion en date lisible)
            for LocalTimestamp in Message.findall("LocalTimestamp"):
                if LocalTimestamp.text is None:
                    Insert = u""
                    DateInsert = u""
                else:
                    Insert = LocalTimestamp.text[:13]
                    DateInsert = datetime.datetime.fromtimestamp(int(LocalTimestamp.text[:10])).strftime('%Y-%m-%d %H:%M:%S')

                StrMessage = StrMessage + u' date="' + Insert + u'" readable_date="' + DateInsert + u'" '


            # Complément des champs non présents dans la sauvegarde windows
            StrMessage = StrMessage + u'toa="null"  sc_toa="null" service_center="+33695000641" read="1" status="-1" locked="0" contact_name="" />'


            file.write(StrMessage.encode('utf8'))

        # Fermuture du fichier XML
        file.write('<smses>')

        file.close()

except IOError as e:
    print("({})".format(e))
