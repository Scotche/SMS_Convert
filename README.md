# SMS_Convert

## Petit outil Python en ligne de commande
Permet la conversion d’un fichier de sauvegarde de sms en provenance d’un Windows Phone en un fichier XML classique compatible avec une récupération sous Android.

## Usage
convert.py sauvegarde.msg

## Pré-requis nécessaires

Ce script est prévu pour Python 2.7, il nécessite les bibliothèques suivantes :
- xml
- datetime
- argparse

## Outils extérieurs

La sauvegarde windows est à effectuer avec l'outil 
[contacts+message backup](https://www.microsoft.com/en-us/store/apps/contacts-message-backup/9nblgggz57gm)

La restauration sous Andriod est à effectuer avec [SMS Backup & Restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore)

# SMS_Convert

## Small Python command line tool


## Requirement

xml
datetime
argparse